//
//  Requester.swift
//  Queries
//
//  Created by Lora Kucher on 11/10/18.
//

import UIKit
import Alamofire
import SystemConfiguration

extension Dictionary {
    var json: Data {
//        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return jsonData/*String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson*/
        } catch {
            return Data()
        }
    }
    
    func printJson() {
//        print(json)
    }
    
}


class Requester: NSObject {
    
    static let shared = Requester()
    var sessionManager = Alamofire.SessionManager()
    private var reachabilityManager: NetworkReachabilityManager? = nil
    
    override init() {
        super.init()
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIConfigs.timeoutInterval
        sessionManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    private static var manager: Alamofire.SessionManager {
        return shared.sessionManager
    }
    
    class func sendRequest(request requestPath: String, method: Alamofire.HTTPMethod, parameters: Parameters? = nil, headers: Bool = false, completion: @escaping([String: Any]) -> Void) {
        let utilityQueue = DispatchQueue.global(qos: .utility)
        let encoding: ParameterEncoding = URLEncoding.httpBody
        let checkHeaders = APIConfigs.header //headers ? APIConfigs.header : nil
        
        var request = URLRequest(url: URL(string: requestPath)!)
        request.httpMethod = method.rawValue
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        let pjson = parameters?.json
//        if JSONSerialization.isValidJSONObject(parameters!) {
//            print("Valid Json")
//        } else {
//            print("InValid Json")
//        }
        
//        let data = (parameters?.data(using: .utf8))! as Data
        request.httpBody = pjson
        
        if method == .get {
            
            manager.request(requestPath, method: method, parameters: parameters).responseString(queue: utilityQueue) { (response) in
                
                DispatchQueue.main.async {
//                    print(response)
                    switch response.result {
                        
                    case .success(let string):
//                        print(string)
                        let result: [String: Any] = ["RESPONSE":response]
                        completion(result)
                        
                    case .failure(let error):
                        print(error)
                    }
                }
            }
            
        } else {
            manager.request(request).responseJSON(queue: utilityQueue) { (response) in
                ResponseValidator.checkResponse(response: response, completion: { response in
                    DispatchQueue.main.async {
                        completion(response)
                    }
                })
//                print(response)
            }
        }
    }
    
    // this function creates the required URLRequestConvertible and NSData we need to use Alamofire.upload
    class func urlRequestWithComponents(urlString:String, parameters:Dictionary<String, String>, imageData:Data) -> (URLRequest, Data) {

        // create url request to send
        var mutableURLRequest = URLRequest(url: NSURL(string: urlString)! as URL)
        mutableURLRequest.httpMethod = Alamofire.HTTPMethod.post.rawValue
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")



        // create upload data to send
        let uploadData = NSMutableData()

        // add image
        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: .utf8)!)
        uploadData.append("Content-Disposition: form-data; name=\"file\"; filename=\"file.png\"\r\n".data(using: .utf8)!)
        uploadData.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        uploadData.append(imageData as Data)

        // add parameters
        for (key, value) in parameters {
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: .utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: .utf8)!)
        }
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: .utf8)!)



        // return URLRequestConvertible and NSData
        var finalRequest: (URLRequest, Data)!
        do {
            try finalRequest = (Alamofire.URLEncoding().encode(mutableURLRequest, with: nil), uploadData as Data)
        } catch {
            print("Error of formation request: \(error)")
        }
        return finalRequest
        //        let urlRequest = urlRequestWithComponents(urlString: uploadPath, parameters: parameters, imageData: imageData)

        //        manager.uplo upload(urlRequest.1, to: urlRequest.0 as! URLConvertible)
        //            .uploadProgress(closure: { (progress) in
        //                print("Upload Progress: \(progress.fractionCompleted)")
        //            })
        //            .responseString(completionHandler: { (response) in
        //                print(response.result)
        //                completion(response)
        //            })
    }
    
    class func sendUpload(upload uploadPath: String, parameters: Parameters? = nil, image: Data? = nil, completion: @escaping(DataResponse<String>) -> Void) {
        guard let parameters = parameters else {
            print("this upload request has no parameters")
            return
        }
        let httpHeaders = ["Content-Type": "multipart/form-data"]

        manager.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
            }
            if let imageData = image {
                multipartFormData.append(imageData, withName: "upload", fileName: "attachment_file.jpg", mimeType: "image/*")
            }
        },
                       to: uploadPath,
                       method: .post,
                       headers: httpHeaders,
                       encodingCompletion: { (result) in
                        switch result {
                        case .success(let upload, _, _):

                            upload.uploadProgress(closure: { (progress) in
//                                print("Upload Progress: \(progress.fractionCompleted)")
                            })

                            upload.responseString(completionHandler: { (response) in
//                                print(response.result)
                                completion(response)
                            })

                        case .failure(let encodingError):
                            print(encodingError)
                        }
        })

    }
    
}
