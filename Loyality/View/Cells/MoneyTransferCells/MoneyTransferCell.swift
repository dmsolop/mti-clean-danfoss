//
//  MoneyTransferCell.swift
//  Loyality
//
//  Created by Димон on 07.07.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit

class MoneyTransferCell: UITableViewCell {
    
    @IBOutlet weak var codecIDLabel: UILabel!
    @IBOutlet weak var payDateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var subViewHeight: NSLayoutConstraint!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imegeViewTop: NSLayoutConstraint!
    @IBOutlet weak var imageViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var answer2ImageView: UIImageView!
    @IBOutlet weak var answer2Label: UILabel!
    @IBOutlet weak var answer1Label: UILabel!
    
    var imageCrop: CGFloat?
    
//    var payment: UserPayment? {
//        didSet {
//            if let p = payment {
//                setUpCell(payment: p)
//            }
//        }
//    }
//    
//    var isShowed = false
//    {
//        didSet {
//            if let payment = self.payment {
//                subViewHeight.constant = isShowed ? 370 : 0
//                imageViewHeight.constant = isShowed ? 200 : 0
//                setUpCell(payment: payment)
//                self.tableView?.reloadData()
//            }
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        subViewHeight.constant = 0
        
    }

//
//    private func getSelfIndexPath() -> IndexPath? {
//        var myIndexPath: IndexPath!
//        self.indexPath.flatMap { myIndexPath = $0 }
//        return myIndexPath
//    }
    
//    //MARK: - Set Up Cell
//    func setUpCell(payment: UserPayment) {
//        codecIDLabel.text = payment.codecID
//        payDateLabel.text = payment.payDate
//        let amount = String(payment.amount)
//        amountLabel.text = "+" + amount + " грн"
//
//        if let answer = payment.answer1, answer != "" {
//         answer1Label.text = "Відповідь: " + answer
//        } else {
//           answer1Label.text = ""
//        }
//
//        if payment.messageText.first == "<" {
//            textView.attributedText = payment.messageText.convertHtml()
//        } else {
//            textView.text = payment.messageText
//        }
//
//        if let answer2 = payment.answer2 {
//            if answer2.contains("http") {
//                answer2Label.text = ""
//                answer2ImageView.sd_setImage(with: URL(string: answer2)) { (image, error, casheImage, url) in
//                    DispatchQueue.main.async {
//                        if let err = error {
//                            print(err)
//                            self.answer2ImageView.isHidden = true
//                            self.imageViewHeight.constant = 0
//                        } else {
//                            self.imageCrop = image?.getCropRatio()
//                            self.answer2ImageView.isHidden = false
////                            if let crop = self.imageCrop {
////                                let width = self.frame.size.width
////                                self.imageViewHeight.constant = 200
////                            }
//                        }
//                    }
//                }
//            } else {
//                answer2Label.text = "Зіскановано: " + answer2
//                imageViewHeight.constant = 0
//            }
//        } else {
//            answer2Label.text = ""
//            imageViewHeight.constant = 0
//        }
//
//    }
//
}
