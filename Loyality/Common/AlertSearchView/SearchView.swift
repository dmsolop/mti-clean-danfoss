//
//  SearchView.swift
//  Loyality
//
//  Created by Димон on 15.04.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit

class SearchView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func setupUI() {
        backgroundColor = UIColor.clear
        setupSubViews()
        setupAutoLayout()
    }

    // MARK: Initialize UI Elements
    private let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.isTranslucent = false
        searchBar.barTintColor = UIColor.newDesignBlue
        searchBar.layoutIfNeeded()
        return searchBar
    }()

    private let searchImageView: UIImageView = {
        let image = UIImage(named: "search_white")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var searchBarTextField: UITextField = { [unowned self] in
        let textField = self.searchBar.value(forKey: "searchBarTextField") as! UITextField
        textField.font = UIFont(name: "Helvetica Neue", size: 20)
        textField.textColor = UIColor.darkGray
        textField.tintColor = UIColor.newDesignBlue
        textField.clearButtonMode = .never
        textField.leftViewMode = .never
        textField.leftView = nil
        textField.rightViewMode = .always
        textField.rightView = self.searchImageView
        return textField
    }()

    private func setupSubViews() {
        addSubview(searchBar)
        searchBar.addSubview(searchBarTextField)
        searchBarTextField.addSubview(searchImageView)
    }

    private func setupAutoLayout() {
        searchImageView.image = searchImageView.image?.resizeImage(CGFloat(20), opaque: true)
    }
}
