//
//  MTICipher.m
//  Loyality
//
//  Created by Denis Romashov on 3/22/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

#import "MTICipher.h"
#import "Extensions.h"
//#import <RNCryptor/RNCryptor.h>
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonHMAC.h>

@implementation MTICipher

//+ (NSData *)doCipher:(NSData *)dataIn
//                 key:(NSData *)symmetricKey
//             context:(CCOperation)encryptOrDecrypt // kCCEncrypt or kCCDecrypt
//{
//    CCCryptorStatus ccStatus   = kCCSuccess;
//    size_t          cryptBytes = 0;    // Number of bytes moved to buffer.
//    NSMutableData  *dataOut    = [NSMutableData dataWithLength:dataIn.length + kCCBlockSizeAES128];
//
//    ccStatus = CCCrypt( encryptOrDecrypt,
//                       kCCAlgorithmAES128,
//                       kCCOptionPKCS7Padding | kCCOptionECBMode,
//                       symmetricKey.bytes,
//                       kCCKeySizeAES128,
//                       0,
//                       dataIn.bytes, dataIn.length,
//                       dataOut.mutableBytes, dataOut.length,
//                       &cryptBytes);
//
//    if (ccStatus != kCCSuccess) {
//        NSLog(@"CCCrypt status: %d", ccStatus);
//    }
//
//    dataOut.length = cryptBytes;
//
//    return dataOut;
//}

+ (NSData *)encrypt:(NSString *)message key:(NSData *)key {
    NSData *toencrypt = [message dataUsingEncoding:NSASCIIStringEncoding];
    
//    NSData *pass = [@"1234567891123456" dataUsingEncoding:NSASCIIStringEncoding];
    
    NSData *iv = [@"1010101010101010" dataUsingEncoding:NSASCIIStringEncoding];
    
    CCCryptorStatus status = kCCSuccess;
//    [toencrypt data]
    
    NSData *encrypted = [toencrypt dataEncryptedUsingAlgorithm:kCCAlgorithmAES128 key:key initializationVector:iv options:(kCCOptionPKCS7Padding | kCCOptionECBMode) error:&status];
    
//    NSString *text = [NSString base64StringFromData:encrypted length:[encrypted length]];
    return encrypted;
}

//+ (NSData *)encryptDataWithAESECB:(NSData *)data
//                              key:(NSData *) key
//                            error:(NSError **)error {
//
//    size_t outLength;
//
//    int cipherLen = (int)(data.length/kCCencrypetedDataAlgorithmBlowfish + 1)*kCCAlgorithmBlowfish;
//    NSMutableData *cipherData = [NSMutableData dataWithLength:cipherLen];
//    NSData *newData = [self addPaddingBeforeEncryptWithAESECB:data];
//
//    CCCryptorStatus result = CCCrypt(kCCEncrypt, // operation
//                                     kAlgorithm, // Algorithm
//                                     kCCOptionECBMode, // Mode
//                                     key.bytes, // key
//                                     key.length, // keylength
//                                     0,// iv
//                                     newData.bytes, // dataIn
//                                     newData.length, // dataInLength,
//                                     cipherData.mutableBytes, // dataOut
//                                     cipherData.length, // dataOutAvailable
//                                     &outLength); // dataOutMoved
//
//
//    if (result == kCCSuccess) {
//        cipherData.length = outLength;
//    }else {
//        if (error) {
//            *error = [NSError errorWithDomain:kRNCryptManagerErrorDomain code:result userInfo:nil];
//        }
//        return nil;
//    }
//    return cipherData;
//}

@end
