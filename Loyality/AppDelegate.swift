//
//  AppDelegate.swift
//  Loyality
//
//  Created by Denis Romashov on 1/5/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import FirebaseCore
import FirebaseMessaging
import UserNotifications

let notificationCategory = "newCategory"


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    let pepsellMessageIDKey = "com.bonussystem.pepsell"
    let locationManager = CLLocationManager()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        locationManager.requestAlwaysAuthorization()

        rootTransactiom()
        
        GMSServices.provideAPIKey(APIConfigs.apiKey)
        
        Security.generateKeyPairsIfNeeded {
//            print("PublicKey:")
//            print(Security.publicKey())
//            print("PrivateKey:")
//            print(Security.privateKey()!)
        }

        registerForPushNotifications()
        application.registerForRemoteNotifications()
        
        return true
    }
    
    
    private func rootTransactiom() {
        //        window?.rootViewController = StoryboardScene.Main.storyboard.instantiateInitialViewController()
        
        let vc = (Session.pin != nil) ? StoryboardScene.Main.pinViewController.instantiate() : StoryboardScene.Main.registrationViewController.instantiate()
        
        let nc = UINavigationController(rootViewController: vc)
        window?.rootViewController = nc
        window?.makeKeyAndVisible()
    }
    
    //MARK: - Push notification
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            FirebaseApp.configure()
            Messaging.messaging().delegate = self
            let newCategory = UNNotificationCategory(identifier: notificationCategory,
                                                     actions: [],
                                                     intentIdentifiers: [],
                                                     options: [])
            
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.setNotificationCategories([newCategory])
            center.requestAuthorization( options: [.alert, .badge, .sound]) {granted, error in
                    guard error == nil else{
                        print(error!.localizedDescription)
                        return
                    }
            }
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
        UNMutableNotificationContent().sound = UNNotificationSound.default
        
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo[pepsellMessageIDKey] {
            print("Message ID: \(messageID)")
        }
    }
    
    @available(iOS 10, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo
        
        var vc = self.window!.rootViewController

        if vc is UINavigationController {
            vc = (vc as! UINavigationController).visibleViewController
        }
        
        if !(vc is ChatViewController) {
            // Change this to your preferred presentation option
             
        }

    }
    
    //Messeging
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        if let phone = Session.phone {
            Communicator.sendFcmKey(phone: phone, fcmKey: fcmToken) {}
        }
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: .fcmToken, object: nil, userInfo: dataDict)
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
        scheduleNotification(message: remoteMessage) { (success) in
            if success {
                print("******We send push*****")
            } else {
                print("****Failed push****")
            }
        }
    }
    
    func scheduleNotification(message: MessagingRemoteMessage, completeion: @escaping (Bool) -> ()) {
        let identifier = Session.triggerIdentifire
//        removeNotifications(withIdentifires: [identifier])
        
        let fromUserId = message.appData["FROM_USER_ID"] as! String
        let title:String = fromUserId == Session.serverPhone ? "Epicentr" : fromUserId
        let body:String = message.appData["TEXT"] as! String
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default
        content.categoryIdentifier = notificationCategory
        
        if let imageUrl = message.appData["IMAGE"],
            let fileURL = URL(string: imageUrl as! String),
            let attachement = try? UNNotificationAttachment(identifier: "attachment", url: fileURL, options: nil) {
            content.attachments = [attachement]
        }
        
        let date = Date(timeIntervalSinceNow: 3)
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.day, .hour, .minute, .second], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error) in
            completeion(error == nil)
        }
    }
    
    func removeNotifications(withIdentifires identifires: [String]) {
        
         let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: identifires)
    }

}
