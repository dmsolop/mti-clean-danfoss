//
//  PopUpMenuVC.swift
//  Loyality
//
//  Created by Димон on 07.07.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit

protocol PopUpMenuDelegate {
    func sendingCurrentViewController(vc: UIViewController)
    func selectTabBarItem(index: Int)
}

class PopUpMenuVC: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    
    var delegate: PopUpMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.image = UIImage().getSavedImageByName(name: "avatarImage.png") ?? UIImage(named: "avatar_bg")
    }
    

    //MARK: - Routing
    static func storyboardInstance() -> PopUpMenuVC? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: PopUpMenuVC.self)) as? PopUpMenuVC
    }
    
    func getAvatarImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
    //MARK: - Actions
    @IBAction func partnersClicked(_ sender: UIButton) {
        self.delegate?.selectTabBarItem(index: 0)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func messengerCliced(_ sender: UIButton) {
        self.delegate?.selectTabBarItem(index: 1)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func userPaymentsClicked(_ sender: UIButton) {
        self.delegate?.selectTabBarItem(index: 2)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func trainingClicked(_ sender: UIButton) {
        self.delegate?.selectTabBarItem(index: 3)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func editProfile(_ sender: UIButton) {
        if let profileVC = ProfileViewController.storyboardInstance() {
            self.delegate?.sendingCurrentViewController(vc: profileVC)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func appSettingsClicked(_ sender: UIButton) {
        if let settingsVC = AppSettingsVC.storyboardInstance() {
            self.delegate?.sendingCurrentViewController(vc: settingsVC)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func gestureRecognizerTapped(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
