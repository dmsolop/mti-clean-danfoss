//
//  TabBarController.swift
//  Loyality
//
//  Created by Димон on 15.07.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    @IBOutlet weak var myTabBar: UITabBar!
        
    enum TabBarTabs {
        typealias RawValue    = Int
        static let home       = 0
        static let messanger  = 1
        static let balance    = 2
        static let training   = 3
    }
    
    var tabsTag: Int = 0
    
    // Override selectedViewController for User initiated changes
    override var selectedViewController: UIViewController? {
        didSet {
            tabChangedTo(selectedIndex: selectedIndex)
        }
    }
    // Override selectedIndex for Programmatic changes
    override var selectedIndex: Int {
        didSet {
            tabChangedTo(selectedIndex: selectedIndex)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        tabBarItem.title = ""
        setTabBarItems()
        setUpGesturs()
    }
    
    func setTabBarItems() {
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 12)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 15)!], for: .selected)

         let myTabBarItem1 = (self.tabBar.items?[0])! as UITabBarItem
        myTabBarItem1.image = UIImage(named: "home")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem1.selectedImage = UIImage(named: "home")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
         myTabBarItem1.title = "Головна"
         myTabBarItem1.imageInsets = UIEdgeInsets(top: 33, left: 0, bottom: 30, right: 0)

         let myTabBarItem2 = (self.tabBar.items?[1])! as UITabBarItem
        myTabBarItem2.image = UIImage(named: "messenger")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem2.selectedImage = UIImage(named: "messenger")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
         myTabBarItem2.title = "Месенджер"
         myTabBarItem2.imageInsets = UIEdgeInsets(top: 33, left: 0, bottom: 30, right: 0)


         let myTabBarItem3 = (self.tabBar.items?[2])! as UITabBarItem
        myTabBarItem3.image = UIImage(named: "ballance")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem3.selectedImage = UIImage(named: "ballance")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
         myTabBarItem3.title = "Зарахування"
         myTabBarItem3.imageInsets = UIEdgeInsets(top: 33, left: 0, bottom: 30, right: 0)

         let myTabBarItem4 = (self.tabBar.items?[3])! as UITabBarItem
        myTabBarItem4.image = UIImage(named: "training")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem4.selectedImage = UIImage(named: "training")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
         myTabBarItem4.title = "Навчання"
         myTabBarItem4.imageInsets = UIEdgeInsets(top: 33, left: 0, bottom: 30, right: 0)

    }
    
    func tabChangedTo(selectedIndex: Int) {
        tabBar.tintColor = setColorByTabBarPosition(tabBarPosition: selectedIndex)
//        if selectedIndex == 1 {
//            if let vc = selectedViewController as? ChatViewController {
//                vc.chatHistory(withDelay: UInt32(0.5))
//            }
//        }
    }
    
    func setColorByTabBarPosition(tabBarPosition:Int) -> UIColor {
        switch tabBarPosition {
        case TabBarTabs.home:
            return ColorPalette.homeColor
        case TabBarTabs.messanger:
            return ColorPalette.messangerColor
        case TabBarTabs.balance:
            return ColorPalette.balanceColor
        case TabBarTabs.training:
            return ColorPalette.trainingColor
        default:
            return UIColor.clear
        }
    }
    
    func setUpGesturs() {
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 15)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 15)!], for: .selected)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    @objc func handleSwipeGesture(_ gesture: UISwipeGestureRecognizer) {
        guard let viewControllers = self.viewControllers else { return }
        let tabs = viewControllers.count
        if gesture.direction == .left {
            if self.selectedIndex < tabs - 1 {
                self.selectedIndex += 1
            } else if self.selectedIndex == tabs - 1 {
                self.selectedIndex = 0
            }
        } else if gesture.direction == .right {
            if self.selectedIndex > 0 {
                self.selectedIndex -= 1
            } else if self.selectedIndex == 0 {
                self.selectedIndex = tabs - 1
            }
        }
    }
}

extension TabBarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return MyTransition(viewControllers: tabBarController.viewControllers)
    }
    
}

