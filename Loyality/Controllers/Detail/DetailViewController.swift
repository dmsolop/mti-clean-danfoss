//
//  DetailViewController.swift
//  Loyality
//
//  Created by Denis Romashov on 1/5/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//


import UIKit
import SDWebImage
import LTHRadioButton
import PureLayout



class DetailViewController: BaseViewController {
    
    var partner: Partner!
    var pins = [MapObject]()
    //    var tabs = ["Menu TAB 1","Menu TAB 2","Menu TAB 3","Menu TAB 4","Menu TAB 5","Menu TAB 6"]
    var tabItems:[TabItem] = [TabItem(title: "ПОПОВНИТИ", type: .send)]
    var currentIndex: Int = 0
    
    var radioButtons = [LTHRadioButton]()
    var refillAmount = ""
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var menuBarView: MenuTabsView!
    @IBOutlet private weak var headerView: UIView!
    @IBOutlet private weak var logoImageView: UIImageView!
    
    @IBOutlet var refillView: UIView!
    
    @IBOutlet weak var refillOkBtn: UIButton!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var refillStackView: UIStackView!
    private weak var pageViewController: UIPageViewController?
    
    private var tap: UITapGestureRecognizer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.sendSubviewToBack(visualEffectView)
        visualEffectView.alpha = 0.4
        
        refillView.layer.cornerRadius = 6
        setupRefillView()
        
        headerView.backgroundColor = partner.circleColor()
        logoImageView.sd_setImage(with: URL(string: partner.circleLogoBig), completed: nil)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTapBack))
        
        if partner.barcodes.isEmpty == false || partner.circleLogo.contains("fishka") {
            tabItems.append(TabItem(title: "БОНУСИ", type: .card))
        }
        
        tabItems.append(TabItem(title: "МАПА", type: .map))
        
        menuBarView.dataArray = tabItems
        menuBarView.collView.backgroundColor = partner.circleColor()
        menuBarView.isSizeToFitCellsNeeded = true
        menuBarView.collView.reloadData()
        menuBarView.menuDelegate = self
        
        menuBarView.collView.selectItem(at: IndexPath.init(item: 0, section: 0), animated: true, scrollPosition: .centeredVertically)
        pageViewController?.setViewControllers([viewController(At: 0)!], direction: .forward, animated: true, completion: nil)
        pageViewController?.delegate = self
        addNotificationCenter()
        
        //        menuBarView.collView.backgroundColor = UIColor.init(white: 0.97, alpha: 0.97)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPageController" {
            pageViewController = segue.destination as? UIPageViewController
        }
    }
    
    @objc func handleTapBack(sender: UITapGestureRecognizer? = nil) {
        animateOut()
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        if let sender = sender {
            for btn in radioButtons {
                if btn.tag == sender.view?.tag {
                    btn.select(animated: true)
                    if let subviews = sender.view?.subviews {
                        for item in subviews {
                            if item .isKind(of: UILabel.self) {
                                refillAmount = (item as! UILabel).text!
                            }
                        }
                    }
                    
                } else {
                    btn.deselect(animated: true)
                }
            }
        }
    }
    
    func setupRefillView() {
        refillView.backgroundColor = partner.circleColor()
        refillOkBtn.tintColor = partner.circleColor()
        
        if let partner = self.partner {
            for (index, item) in partner.nominal.enumerated() {
                
//                let rowStackView = UIStackView()
//                rowStackView.axis = .horizontal
//                rowStackView.distribution = .fillProportionally
//                rowStackView.alignment = .fill
//                rowStackView.spacing = 0
                
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
                tapGesture.delegate = self as? UIGestureRecognizerDelegate
                let radioButton = LTHRadioButton(diameter: 20, selectedColor: UIColor.gray, deselectedColor: UIColor.gray)
                let label = UILabel()
                let view1 = UIView()
                
                view1.backgroundColor = UIColor.white
                
                label.text = String(item)
                label.textAlignment = .center
                label.textColor = UIColor.black
                
                label.tag = index+1
                view1.tag = index+1
                radioButton.tag = index+1
                
                radioButtons.append(radioButton)
                
                view1.addSubview(radioButton)
                view1.addSubview(label)
                view1.addGestureRecognizer(tapGesture)
                view1.isUserInteractionEnabled = true
                
                radioButton.autoPinEdge(toSuperviewEdge: .left, withInset: 50)
                radioButton.autoPinEdge(toSuperviewEdge: .top, withInset: 15)
                radioButton.autoAlignAxis(.horizontal, toSameAxisOf: view1, withOffset: 0)
                
                label.autoAlignAxis(.horizontal, toSameAxisOf: view1, withOffset: 0)
                label.autoAlignAxis(.vertical, toSameAxisOf: view1, withOffset: 0)
                label.autoPinEdge(toSuperviewEdge: .right)
                
                refillStackView.addArrangedSubview(view1)
            }
        }
    }
    
    func addNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(onShowPopUpDialog(_:)), name: .postNotificationForPopUpDialog, object: nil)
    }
    
    @objc func onShowPopUpDialog(_ notification:Notification) {
        animateIn()
    }
    
    @IBAction func refillBtnOkDidPush(_ sender: UIButton) {
        animateOut()
        let refillValue = ["value": refillAmount]
        NotificationCenter.default.post(name: .postNotificationForSetRefillingValue, object: self, userInfo: refillValue)
        
    }
    
    func animateIn() {
        self.view.bringSubviewToFront(visualEffectView)
        self.view.addGestureRecognizer(tap)
        self.view.addSubview(refillView)
        refillView.center = self.view.center
        
        refillView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        refillView.alpha = 0
        
        UIView.animate(withDuration: 0.7) {
            self.refillView.alpha = 1
            self.refillView.transform = CGAffineTransform.identity
            
        }
    }
    
    func animateOut() {
        UIView.animate(withDuration: 0.7, animations: {
            self.refillView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.refillView.alpha = 0
            
            self.view.sendSubviewToBack(self.visualEffectView)
        }) { (success:Bool) in
            self.refillView.removeFromSuperview()
            self.view.removeGestureRecognizer(self.tap)
        }
    }
    
    func viewController(At index: Int) -> UIViewController? {
        if((self.menuBarView.dataArray.count == 0) || (index >= self.menuBarView.dataArray.count)) {
            return nil
        }
        currentIndex = index
        let tabItem = tabItems[index]
        switch tabItem.type {
        case .send:
            let sendVC = StoryboardScene.Main.sendController.instantiate() as! SendViewController
            sendVC.partner = partner
            return sendVC
        case .card:
            let codeVC = StoryboardScene.Main.cardController.instantiate() as! BarCodeViewController
            codeVC.partner = partner
            return codeVC
        case .map:
            let mapVC = StoryboardScene.Main.mapController.instantiate() as! MapViewController
            mapVC.pins = pins
            return mapVC
        }
        
        //        let contentVC = storyboard?.instantiateViewController(withIdentifier: "ContentVC") as! ContentVC
        //        contentVC.strTitle = tabItems[index]
        //        contentVC.pageIndex = index
        //        return contentVC
    }
}

extension DetailViewController: MenuBarDelegate {
    func menuBarDidSelectItemAt(menu: MenuTabsView, index: Int) {
        if index != currentIndex {
            if index > currentIndex {
                pageViewController?.setViewControllers([viewController(At: index)!], direction: .forward, animated: true, completion: nil)
            } else {
                pageViewController?.setViewControllers([viewController(At: index)!], direction: .reverse, animated: true, completion: nil)
            }
            menuBarView.collView.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
}

extension DetailViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! BasePageViewController).pageIndex()
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index -= 1
        return self.viewController(At: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! BasePageViewController).pageIndex()
        
        if (index == tabItems.count) || (index == NSNotFound) {
            return nil
        }
        
        index += 1
        return self.viewController(At: index)
    }
}

extension Notification.Name {
    static let postNotificationForPopUpDialog = Notification.Name("postNotificationForShowPopUpDialog")
    static let postNotificationForSetRefillingValue = Notification.Name("postNotificationForSetRefillingValue")
}
