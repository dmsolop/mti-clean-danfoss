//
//  CardViewController.swift
//  Loyality
//
//  Created by Denis Romashov on 4/7/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import LTHRadioButton

class SendViewController: BasePageViewController {
    
    @IBOutlet weak private var cardView: UIView!
    @IBOutlet weak private var currentBalance: UILabel!
    @IBOutlet weak private var totalBalance: UILabel!
    @IBOutlet weak private var sendButton: UIButton!
    @IBOutlet weak private var amountTextField: UITextField!
    @IBOutlet weak private var numberTextField: UITextField!
    
    var typeTextField:TextFieldType!
    var isValidAmount = false
    var isValid = false {
        didSet {
            sendButton.isUserInteractionEnabled = isValid
        }
    }
    
    var partner: Partner!
    
    private let patterns: [TextFieldType : String] = [.KS : RegEx.kyivstar,
                                                      .Vodafone : RegEx.vodafone,
                                                      .Life : RegEx.life]
    
    enum TextFieldType: String, CaseIterable {
        case KS, Vodafone, Life
        
        static var asArray: [TextFieldType] {return self.allCases}
        
        func asInt() -> Int {
            return TextFieldType.asArray.firstIndex(of: self)!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberTextField.isHidden = !partner.isMobileOperator
        numberTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        amountTextField.layer.cornerRadius = 4
        currentBalance.text = "\(partner.balance)"
        totalBalance.text = "\(Session.balance)"
        cardView.backgroundColor = partner.circleColor()
        
        amountTextField.delegate = self
        numberTextField.delegate = self
        checkIsDiscretPartnerHasNominals()
        addNotificationCenter()
    }
    
    func addNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(setRefillValue(_:)), name: .postNotificationForSetRefillingValue, object: nil)
    }
    
    @objc func setRefillValue(_ notification:Notification) {
        if let data = notification.userInfo as? [String: String] {
            for (_, value) in data {
                amountTextField.text = value
            }
        }
        
    }
    
    @IBAction private func sendButtonPressed(_ sender: Any) {
        
        let amount = Int(amountTextField?.text ?? "0") ?? 0
        if amount > Session.balance {
            ServerError.show(alert: "Сума перевищує баланс")
            return
        } else if amount == 0 {
            ServerError.show(alert: "Сума поповнення повинна бути більшою 0")
            return
        }
        

        var phone: String = ""
        if partner.isMobileOperator, isAllFieldsValid(partner: partner)?.0 ?? false {
            phone =  numberTextField.text ?? "380"
        }
        
        
        view.endEditing(true)
        showLoader()
        Communicator.payment(phone: Session.phone!, partner: partner.id, amount: amount, phoneTopUp: phone) { [weak self] (response) in
            ServerError.show(alert: "Очікуйте на оплату")
            Session.balance -= amount
            
            self?.hideLoader()
            self?.amountTextField.text = nil
            self?.numberTextField.text = nil
            self?.totalBalance.text = "\(Session.balance)"
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        guard let value = isAllFieldsValid(partner: partner) else { return }
        isValid = value.0
        
        textField.layer.cornerRadius = 4
        textField.layer.borderWidth = 2
        textField.layer.borderColor = (isValid) ? UIColor.lightBlue.cgColor : UIColor.red.cgColor
    }
    
    private func checkIsDiscretPartnerHasNominals() {
        if let partner = partner, partner.isDiscrete,
        ((partner.nominal.count == 1 && partner.nominal.first == 0) ||
        partner.nominal.isEmpty) {
            amountTextField.isEnabled = false
        }
    }
    
    private func isAllFieldsValid(partner:Partner) -> (Bool, TextFieldType)? {
        var isValid = true
        
        switch partner.name {
        case TextFieldType.KS.rawValue:
            typeTextField = TextFieldType(rawValue: TextFieldType.KS.rawValue)
        case TextFieldType.Vodafone.rawValue:
            typeTextField = TextFieldType(rawValue: TextFieldType.Vodafone.rawValue)
        case TextFieldType.Life.rawValue:
            typeTextField = TextFieldType(rawValue: TextFieldType.Life.rawValue)
        default: break
        }
        
        guard let pattern = patterns[typeTextField] else { return nil }
        
        if numberTextField.text?.checkToPattern(pattern: pattern) ?? false {
            isValid = true
        } else {
            isValid = false
        }
        return (isValid, typeTextField)
    }
}

extension SendViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if partner.isDiscrete && !partner.nominal.isEmpty {
            self.view.endEditing(true)
            NotificationCenter.default.post(name: .postNotificationForPopUpDialog, object: self, userInfo: nil)
            isValid = true
            
        } else if partner.isMobileOperator, textField.tag == 1, textField.text?.count ?? 0 == 0 {
            textField.text = "380"
        } else {
            isValid = true
        }
    }
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            textField.layer.borderColor = UIColor.simpleGray.cgColor
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField.tag {
        case 0:
            textField.resignFirstResponder()
            self.numberTextField.becomeFirstResponder()
            return true
        case 1:
            textField.resignFirstResponder()
            return true
        default:
            return false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 12
    }
}

