//
//  BasePageViewController.swift
//  Loyality
//
//  Created by Denis Romashov on 4/7/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit

protocol PageViewProtocol {
    func pageIndex() -> Int
}

class BasePageViewController: UIViewController {
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}

extension BasePageViewController: PageViewProtocol {
    func pageIndex() -> Int {
        return index
    }
}
