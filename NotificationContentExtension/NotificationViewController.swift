//
//  NotificationViewController.swift
//  NotificationContentExtension
//
//  Created by Dima on 8/23/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet var label: UILabel?
    @IBOutlet weak var mainImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }
    
    func didReceive(_ notification: UNNotification) {
        self.label?.text = notification.request.content.body
        let urlImage = notification.request.content.attachments[0]
        print("****Url Image = \(urlImage)")
//        mainImage.sd_setImage(with: urlImage, completed: nil)
    }

}
